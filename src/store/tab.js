export default {
    state:{
        isCollapse:false,//控制菜单的展开还是收起
        tabList:[
            {
                path:"/",
                name:"homepage",
                label:"首页",
                icon:"setting",
                url:"../views/homepage.vue"
            }
        ]
    },
    mutations:{
        //修改菜单的展开还是收起
        collapseMenu(state){
            state.isCollapse = !state.isCollapse
        },
        selectMenu(state,val){
            console.log(val)
            if(val !== "homepage"){
              const index =state.tabList.findIndex(item =>  item.name === val.name)
                if(index === -1){
                    state.tabList.push(val)
                }
            }
        }
    }
}