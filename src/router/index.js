import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
const routes = [
  {
    path: "/Register",
    name: "register",
    component: () => import(/* webpackChunkName: "Login" */ "@/views/Register"),
  },
  {
    path: "/",
    name: "home",
    
    component: () => import(/* webpackChunkName: "Login" */ "@/views/Home"),
    children:[
      {
        path: "",
        name: "homepage",
        component: () => import(/* webpackChunkName: "Login" */ "@/views/Homepage"),
      },
      {
        path: "Commodity",
        name: "commodity",
        component: () => import(/* webpackChunkName: "Login" */ "@/views/Commodity"),
      },
      {
        path: "Category",
        name: "category",
        component: () => import(/* webpackChunkName: "Login" */ "@/views/Category"),
      },
      {
        path: "Orderfor",
        name: "orderfor",
        component: () => import(/* webpackChunkName: "Login" */ "@/views/Orderfor"),
      },
      {
        path: "User",
        name: "user",
        component: () => import(/* webpackChunkName: "Login" */ "@/views/User"),
      },
    ]
  },
  {
    path: "/User",
    name: "user",
    component: () => import(/* webpackChunkName: "Login" */ "@/views/User.vue"),
  },
  {
    path: "/index1",
    name: "index1",
    component: () => import(/* webpackChunkName: "Login" */ "@/views/shiyan/index1.vue"),
  },
  {
    path: "/index2",
    name: "index2",
    component: () => import(/* webpackChunkName: "index2" */ "@/views/shiyan/index2.vue"),
  },
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// router.beforeEach((to, from, next) => {
//   if (to.name !== "login") {
//     if()
//     console.log("*****", document.cookie);
//     next();
//   } else {
//     next();
//   }
// });

export default router;
