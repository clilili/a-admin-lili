import Vue from "vue";
import "normalize.css";
import axios from "axios";
// import http from "@/http";
import * as echarts from 'echarts'
import element from "element-ui";
import App from "./App.vue";
import router from "./router";
import store from "./store";
Vue.config.productionTip = false;
// axios.defaults.headers.post["Content-Type"] =
//   "application/x-www-form-urlencoded";
// axios.defaults.headers.token = localStorage.getItem("token") || "";
// axios.defaults.headers.post["Content-Type"] = "application/form-data";
Vue.prototype.$echarts = echarts;

Vue.prototype.$http = axios;

Vue.use(element);
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");