// module.exports = {
//   publicPath: process.env.NODE_ENV === "production" ? "" : "/",
//   devServer: {
//     port: 3000,
//     // proxy: "http://test.raz-kid.cn",
//     proxy: {
//       "/api": {
//         target: "http://admin.raz-kid.cn",
//       },
//       "/h5": {
//         target: "http://192.168.1.42:3002",
//         pathRewrite: {
//           "^/h5": "",
//         },
//       },
//     },
//   },
// };

module.exports ={
  lintOnSave: false,
  publicPath: process.env.NODE_ENV === "production" ? "" : "/",
  devServer: {
    // port: 3000,
    // proxy: "http://test.raz-kid.cn",
    proxy: {
      "/api": {
        target: "http://admin.raz-kid.cn",
      },
      "/h5": {
        target: "https://fengyong-api.onrender.com",
        pathRewrite: {
          "^/h5": "",
        },
      },
    },
  },
  chainWebpack(config) {
    config.optimization.minimizer('terser').tap((args) => {
    args[0].terserOptions.compress.drop_console = true
     return args
    });
    const cdn = {
      // 访问https://unpkg.com/element-ui/lib/theme-chalk/index.css获取最新版本
      css: ["https://unpkg.com/element-ui@2.10.1/lib/theme-chalk/index.css"],
      js: [
        "https://unpkg.com/vue@2.6.10/dist/vue.min.js", // 访问https://unpkg.com/vue/dist/vue.min.js获取最新版本
        "https://unpkg.com/vue-router@3.0.6/dist/vue-router.min.js",
        "https://unpkg.com/vuex@3.1.1/dist/vuex.min.js",
        "https://unpkg.com/axios@0.19.0/dist/axios.min.js",
        "https://unpkg.com/element-ui@2.10.1/lib/index.js"
      ]
    };

    // 如果使用多页面打包，使用vue inspect --plugins查看html是否在结果数组中
    config.plugin("html").tap(args => {
      // html中添加cdn
      args[0].cdn = cdn;
      return args;
    });
  },
  configureWebpack: config => {
    config.externals = {
      vue: "Vue",
      "element-ui": "ELEMENT",
      "vue-router": "VueRouter",
      vuex: "Vuex",
      axios: "axios"
    };
  },
};
